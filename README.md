# My portfolio

<!--toc:start-->
- [Template](#template)
- [Getting started](#getting-started)
- [Environment variables](#environment-variables)
- [Deployment](#deployment)
- [Valuable resources](#valuable-resources)
- [Todo board](#todo-board)
<!--toc:end-->

This is a React based web page. I know, it's a bit overkill to use React for a
simple portfolio, but like... Leave me alone, OK? I'm learning front-end stuff,
and I'm also not using NEXT or any framework, just Vanilla React.

I'm using React with TypeScript with Vite and TailwindCSS.

## Template

If you are interested in using this project as a template: DON'T >:(

I'm JK, go ahead, it's probably not that pretty anyways.

## Getting started

As a requirement, you'll need `NodeJS` installed, and I think that's all. Maybe
you also need to install `npm`? I'm not even entirely sure, what I AM sure of,
is that you can definitely figure it out on your own. I believe in you.

Clone the repo wherever you want. `CD` into the project directory and run `npm
i` or `npm install` to generate the necessary `node_modules`.

Then, run `npm run dev`, and go to your browser and go to the URL
`127.0.0.1:3000`. To change the port, edit the `port` member in the `server`
object in `vite.config.js`.

## Environment variables

Create a file called `.env` at the project root directory, and add your
environment variables there!

Every environment variables needs to be prefixed with `VITE_`, but you don't
need to add this prefix when referencing the variables in code.

There are no environment variables needed as of now, but in case you want to
configure some API key or something, that would be the way to do it. Here's an
example:

```
VITE_PERSONAL_INFO_EMAIL=youremail@example.com
VITE_PERSONAL_INFO_PHONE=+12222222222
VITE_PERSONAL_INFO_GITLAB=https://gitlab.com/YourUsername
VITE_PERSONAL_INFO_LINKEDIN=https://www.linkedin.com/in/your-profile
```

To add an environment variable in code, modify the file `vite-env.d.ts` and add
it as a `readonly` field in the `ImportMetaEnv` interface. Example:

```
interface ImportMetaEnv {
    readonly VITE_PERSONAL_INFO_EMAIL: string;
    readonly VITE_PERSONAL_INFO_PHONE: string;
    readonly VITE_PERSONAL_INFO_GITLAB: string;
    readonly VITE_PERSONAL_INFO_LINKEDIN: string;
}

interface ImportMeta {
    readonly env: ImportMetaEnv;
}
```

Then, import them anywhere in the code using JS (or TS) like this:

```
const myEmail = import.meta.env.VITE_PERSONAL_INFO_EMAIL;
```

## Deployment

To deploy a simple static React website, I did it on AWS's S3.

- Build the project for prod using `npm run build`, which creates a `dist`
  directory
- Create an S3 bucket on AWS
- Configure the bucket for static web hosting, this should give you a URL for
  accessing the static content (I believe in you)
- Install the AWS CLI (I believe in you)
- Configure an IAM role for the AWS CLI in the management console, with full S3
  permissions
- Configure an Access Key for the new IAM role created
- Configure the CLI credentials using the CLI with the command `aws configure`
- Sync the `dist` directory with the S3 bucket using the command `aws s3 sync
  dist s3://yourbucketname`

## Valuable resources

- [Realtime Colors](https://www.realtimecolors.com/?colors=050315-fbfbfe-2f27ce-dedcff-433bff&fonts=Inter-Inter)
  (made by Juxtopposed)
- [TraversyMedia's React tutorial](https://www.youtube.com/watch?v=LDB4uaJ87e0)
- [TraversyMedia's TailwindCSS tutorial](https://www.youtube.com/watch?v=dFgzHOX84xQ)

## Todo board

- [X] Some way to download my CV in English or Spanish
- [X] Define styling for the 'technologies' page
- [X] Write 'technologies' content to the page
- [X] Fix trashy responsive on small x-axis screens for 'technologies' page
- [X] Get photos of: me, me in GroupM (if possible), me in NUB 7/8 (if
  possible)
- [X] Add stars (max 3) to each item in the 'technologies' page.
- [X] Content for Hobbies page
- [X] Images for Hobbies page
- [X] 'Page under construction' page
- [X] Navbar shows different menu when screen is small
- [X] Deployment
- [X] Deployment documentation
- [ ] Change the pictures on the AboutMe page (especially a pic of myself!)
- [ ] The experience section is WAY too long
- [ ] Extra navlink / page for more of a blogging section
- [ ] Theming (at least light and dark) (ok, it's pretty annoying, as I have to
  add a CSS property `dark:somecolor` to many components)

Jorge's comments

- [X] Improve NavMenu behavior
- [X] Try placing everything in one page only, instead of using NavLinks, use
  AnchorLinks instead
- [X] The About Me page should have little to no personal info, instead, work
  related info only
- [ ] Put a picture of myself in the About Me page
- [ ] Limit the amount of text in almost all of the tabs
- [ ] In the technologies page, try having 2 columns with content, so that the
  user doesn't need to scroll down too much on mobile devices

