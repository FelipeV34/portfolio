/** @type {import('tailwindcss').Config} */

export default {
    content: [
        "./index.html",
        "./src/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {
        extend: {
            fontFamily: {
                sans: ["Roboto", "sans-serif"],
            },
            colors: {
                "clText": "#efecdd",
                "clTextDark": "#131209",
                "clBackground": "#131209",
                "clPrimary": "#cdc697",
                "clPrimaryHover": "#F1E8C5",
                "clSecondary": "#526832",
                "clSecondaryHover": "#6A8239",
                "clSecondaryDark": "#3F5030",
                "clAccent": "#8eb86c",
                "clAccentHover": "#a5cc85",
            },
        },
    },
    plugins: [],
}
