import AboutMeSection from "../components/AboutMeSection";
import EducationSection from "../components/EducationSection";
import ExperienceSection from "../components/ExperienceSection";
import HobbiesSection from "../components/HobbiesSection";
import TechnologiesSection from "../components/TechnologiesSection";

function HeroPage() {
    return (
        <div className="mt-24 mb-12">
            <AboutMeSection />
            <TechnologiesSection />
            <EducationSection />
            <ExperienceSection />
            <HobbiesSection />
        </div>
    );
}

export default HeroPage;
