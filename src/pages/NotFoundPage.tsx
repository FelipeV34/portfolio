import { FaExclamationTriangle } from "react-icons/fa";
import { Link } from "react-router-dom";

function NotFoundPage() {
    return (
        <section className="text-center flex flex-col justify-center items-center h-96">
            <FaExclamationTriangle className="text-6xl text-yellow-400 mb-4 mt-32" />
            <h1 className="text-6xl pb-8">Page not found</h1>
            <Link to="/" className="text-clAccent hover:text-clAccentHover">Back to the home page</Link>
        </section>
    );
}

export default NotFoundPage;
