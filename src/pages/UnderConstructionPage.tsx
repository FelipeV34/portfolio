import { FaTools } from "react-icons/fa";
import { Link } from "react-router-dom";

function UnderConstructionPage() {
    return (
        <section className="text-center flex flex-col justify-center items-center h-96">
            <FaTools className="text-7xl text-clPrimary mb-4 mt-32" />
            <h1 className="text-6xl pb-8">Page under construction</h1>
            <Link to="/" className="text-clAccent hover:text-clAccentHover">Back to the home page</Link>
        </section>
    );
}

export default UnderConstructionPage;
