interface ImportMetaEnv {
    // readonly VITE_PERSONAL_INFO_EMAIL: string;
}

interface ImportMeta {
    readonly env: ImportMetaEnv;
}
