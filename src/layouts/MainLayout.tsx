import { Outlet } from "react-router-dom";
import Navbar from "../components/Navbar";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function MainLayout() {
    return (
        <>
            <Navbar />
            <Outlet />
            <ToastContainer
                position="bottom-right"
                closeOnClick={true}
                toastClassName="bg-black text-clText font-semibold"
            />
        </>
    );
}

export default MainLayout;
