import TextSection from "./TextSection";

const educationParagraphs = [
    <h2 className="text-xl"><b>Konrad Lorenz University Foundation</b></h2>,
    <span>
        I studied mathematics at the Konrad Lorenz University Foundation
        (<i>Fundación</i> <i>Universitaria Konrad Lorenz</i>). There, I learned
        the basics of programming using <b>Java</b> and a little bit of <b>C++</b>,
        scientific computing with <b>Python</b>, relational databases, and of
        course, I also learned a bunch of maths.
    </span>,
    <span>
        I also participated in competitive programming contests with a team
        from my university (programming marathons), with two engineers that
        were extremely talented Java developers. We got as far as to compete at
        the national level.
    </span>,
    <h2 className="text-xl"><b>At work / On my own</b></h2>,
    <span>
        When I was working, I learned frameworks and libraries like
        Python's <b>Django</b>, <b>FastAPI</b>, <b>Pandas</b> / <b>Geo-Pandas</b>, Java's <b>Springboot</b>, and
        CSharp's <b>.NET</b>, as well as a little bit of <b>Typescript</b> with <b>Angular</b>, and
        relational and non-relational databases <b>PostgreSQL</b>, <b>Microsoft
            SQL Server</b> and <b>MongoDB</b>. On my own, I've learned the
        basics of the <b>C programming language</b>, and some Front-End stuff
        like <b>JavaScript</b>, <b>HTML</b>, <b>CSS</b>, <b>Tailwind</b> and <b>React
            JS</b>.
    </span>,
    <h2 className="text-xl"><b>Pontifical Xavierian University</b></h2>,
    <span>
        Right now, I'm looking to get a master's degree in <i>Systems
            engineering and computing</i> in the <i>Pontifical Xavierian
            University</i>, starting in July 2024, where I hope to bridge
        some of the knowledge gaps between me and some of my peers with
        engineering degrees, as well as learn about project management,
        software architecture, etc.
    </span>,
];

function Education() {
    return <TextSection
        title="Education"
        paragraphs={educationParagraphs}
    />;
}

export default Education;
