import { useRef, useState, useEffect } from "react";
import InfoCardElement from "./InfoCardElement";

const email = "felipevanegas11@hotmail.com";
const phone = "+573208006993";
const gitlab = "https://gitlab.com/FelipeV34";
const linkedin = "https://www.linkedin.com/in/felipe-vanegas-84a6b517b/";

function ContactInfoButton() {
    const [isCardVisible, setIsCardVisible] = useState(false);
    const cardRef = useRef<HTMLDivElement>(null);
    const buttonRef = useRef<HTMLButtonElement>(null);

    function handleButtonClick() {
        setIsCardVisible(!isCardVisible);
    }

    function handleOutsideClick(event: MouseEvent) {
        if (
            cardRef.current &&
                !cardRef.current.contains(event.target as Node) &&
                buttonRef.current &&
                !buttonRef.current.contains(event.target as Node)
        ) {
            setIsCardVisible(false);
        }
    }

    useEffect(() => {
        document.addEventListener("mousedown", handleOutsideClick);
        return () => {
            document.removeEventListener("mousedown", handleOutsideClick);
        };
    }, []);

    return (
        <div className="relative z-50">
            <button
            ref={buttonRef}
            className="flex items-center mr-6 py-1 px-6 bg-clPrimary text-clTextDark rounded-full hover:bg-clPrimaryHover whitespace-nowrap"
            onClick={handleButtonClick}
        >
            Contact
        </button>

                {isCardVisible && (
                    <div
                        ref={cardRef}
                        className="fixed top-20 right-4 bg-clBackground p-6 rounded-lg shadow-lg w-96 border border-clPrimary"
                    >

                            <InfoCardElement
                            title="Email:"
                            value={email}
                        />

                            <InfoCardElement
                            title="Phone:"
                            value={phone}
                        />

                            <InfoCardElement
                            title="LinkedIn:"
                            value={linkedin}
                        />

                            <InfoCardElement
                            title="GitLab:"
                            value={gitlab}
                        />

                        </div>
                )}
            </div>
    );
}

export default ContactInfoButton;
