import Experience from "../components/Experience";
import CarouselSection from "./CarouselSection";

const imagePaths = [
    "images/groupm.jpg",
    "images/groupmbeer.jpg",
    "images/groupmpaintball.jpg",
    "images/gsd.webp",
    "images/nub78.jpg",
];

function ExperienceSection() {
    return <CarouselSection
        child={ <Experience /> }
        imagePaths={ imagePaths }
        sectionId="experience"
    />;
}

export default ExperienceSection;
