type ContentSectionProps = {
    child: JSX.Element;
    sectionId: string;
}

function ContentSection({ child, sectionId }: ContentSectionProps) {
    return (
        <section id={ sectionId } className="overflow-x-hidden">
            <div className="container mx-auto flex flex-col px-6 space-y-0 md:flex-row">
                { child }
            </div>
        </section>
    );
}

export default ContentSection;
