import { useState } from "react";
import { MdContentCopy } from "react-icons/md";

type InfoCardElementProps = {
    title: string,
    value: string,
};

function InfoCardElement({ title, value }: InfoCardElementProps) {
    const [copyFeedbackVisible, setCopyFeedbackVisible] = useState(false);

    function copyToClipboard() {
        navigator.clipboard.writeText(value)
            .then(() => {
                setCopyFeedbackVisible(true);
                setTimeout(() => {
                    setCopyFeedbackVisible(false);
                }, 2000);
            })
    }

    return (
        <>
            <p className="mb-2">
                <strong>
                    {title}
                </strong>
            </p>
            <div className="mb-4 flex items-center border p-2 rounded">
                <button
                    className="bg-transparent pl-1 pr-3"
                    onClick={copyToClipboard}
                >
                    <MdContentCopy />
                </button>
                <input
                    type="text"
                    value={value}
                    readOnly
                    className="flex-grow bg-transparent border-none focus:outline-none"
                />
                {copyFeedbackVisible && (
                    <span className="absolute top-0 left-1/2 transform -translate-x-1/2 mt-1 px-2 py-1 bg-clBackground text-clText text-sm rounded">
                        Copied!
                    </span>
                )}
            </div>
        </>
    );
}

export default InfoCardElement;
