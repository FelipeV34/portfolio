import DownloadCv from "./DownloadCv";
import TextSection from "./TextSection";

const aboutMeParagraphs = [
    <span>
        My name is Felipe Vanegas, I am a software engineer from Bogotá -
        Colombia 🇨🇴. I've worked on the industry for ~4 years, focusing
        primarily on Back-End engineering throughout my career, but hey, here I
        am writing a portfolio in React with TypeScript, using TailwindCSS for
        styling (here's the <a className="text-clAccent hover:text-clAccentHover"
            href="https://gitlab.com/Felipev34/portfolio" target="_blank">repo
            for this web page</a>, in case you're interested.)
    </span>,
    <span>
        In terms of programming languages, I have experience working primarily
        with <b>Python</b>, using libraries like Numpy, Pandas and Geo-Pandas,
        and frameworks Django, FastAPI and Flask. I also have some experience
        using <b>Java</b> with Springboot and JPA, and <b>C#</b> with .NET 6.0
        and 8.0, and EFCore.
    </span>,
    <span>
        I have worked with SQL and NoSQL databases,
        namely <b>PostgreSQL</b> and <b>Microsoft SQL Server</b> for SQL,
        and <b>MongoDB</b> for NoSQL. I also have some experience working with
        message queues, such as <b>RabbitMQ</b> and <b>ServiceBus</b>, with
        cloud services <b>AWS</b> and <b>Azure</b>. I also know all
        about <b>Agile</b> methodologies and <b>Scrum</b> methodology, and have
        been learning and applying its principles throughout my career.
    </span>,
    <span>
        My english level is roughly <b>C1</b>, even though I haven't presented
        the TOEFL exam, I did present the IELTS exam in 2019 and got a grade
        of ~75% (which roughly equates to C1). Besides that, I have been
        reading news, watching videos, movies and shows primarily in English
        since I was 16, so it's fair to say my English level is pretty good.
    </span>,
    <span>Download my CV in:</span>,
    <DownloadCv />
];

function AboutMe() {
    return (
        <>
            <TextSection
            title={ "About Me" }
            paragraphs={aboutMeParagraphs}
        />
            </>
    );
}

export default AboutMe;
