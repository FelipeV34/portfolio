import { Link } from "react-router-dom"
import ContactInfoButton from "./ContactInfoButton";
import { useEffect, useRef, useState } from "react";

const linkDivClass = "whitespace-nowrap overflow-hidden text-ellipsis block";
const linkClass = "px-2 rounded-lg hover:bg-clSecondary";

function Navbar() {
    const [isOpen, setIsOpen] = useState(false);
    const menuRef = useRef<HTMLDivElement>(null);
    const menuButtonRef = useRef<HTMLButtonElement>(null);

    function handleClickOutside(event: MouseEvent) {
        if (
            menuRef.current
                && !menuRef.current.contains(event.target as Node)
                && menuButtonRef.current
                && !menuButtonRef.current.contains(event.target as Node)
        ) {
            setIsOpen(false);
        }
    }

    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);

        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, []);

    function toggleOpen() {
        setIsOpen(prevIsOpen => !prevIsOpen);
    }

    function handleNavegationClick() {
        setIsOpen(false);
    }

    return (
        <nav className="bg-clBackground fixed top-0 left-0 w-full h-24 z-40">
            <div className="container mx-auto h-full flex justify-between items-center border-b border-clPrimary">

                <Link className="text-xl m-5" to="/">Felipe Vanegas Q</Link>

                <div className="hidden items-center space-x-6 ml-10 overflow-hidden md:flex">
                    <div className={linkDivClass}>
                        <a className={linkClass} href="/#">Home 🏠</a>
                    </div>
                    <div className={linkDivClass}>
                        <a className={linkClass} href="/#aboutme">About me 🗣️</a>
                    </div>
                    <div className={linkDivClass}>
                        <a className={linkClass} href="/#technologies">Technologies 🖥️</a>
                    </div>
                    <div className={linkDivClass}>
                        <a className={linkClass} href="/#education">Education 📖</a>
                    </div>
                    <div className={linkDivClass}>
                        <a className={linkClass} href="/#experience">Experience 🧑🏻‍💻</a>
                    </div>
                    <div className={linkDivClass}>
                        <a className={linkClass} href="/#hobbies">Hobbies 🥁</a>
                    </div>
                    <ContactInfoButton />
                </div>

                <div className="flex flex-row md:hidden mr-6">
                    <ContactInfoButton />
                    <button
                        ref={menuButtonRef}
                        className={`hamburger block mt-2 focus:outline-none ${isOpen ? "open" : ""}`}
                        onClick={toggleOpen}
                    >
                        <span className="hamburger-top"></span>
                        <span className="hamburger-middle"></span>
                        <span className="hamburger-bottom"></span>
                    </button>
                </div>

            </div>

            <div className="md:hidden" ref={menuRef}>
                <div className={`absolute right-6 flex-col w-96 items-center py-6 mt-6 space-y-6 bg-clBackground border border-clPrimary rounded-lg ${isOpen ? "flex" : "hidden"}`}>
                    <a onClick={handleNavegationClick} href="/#" className={linkClass}>Home 🏠</a>
                    <a onClick={handleNavegationClick} href="/#aboutme" className={linkClass}>About me 🗣️</a>
                    <a onClick={handleNavegationClick} href="/#technologies" className={linkClass}>Technologies 🖥️</a>
                    <a onClick={handleNavegationClick} href="/#education" className={linkClass}>Education 📖</a>
                    <a onClick={handleNavegationClick} href="/#experience" className={linkClass}>Experience 🧑🏻‍💻</a>
                    <a onClick={handleNavegationClick} href="/#hobbies" className={linkClass}>Hobbies 🥁</a>
                </div>
            </div>

        </nav>
    );
}

export default Navbar;
