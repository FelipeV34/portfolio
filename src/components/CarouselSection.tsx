import { ReactElement } from "react";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import ContentSection from "./ContentSection";

type PageProps = {
    child: ReactElement;
    imagePaths: string[];
    sectionId: string;
};

function CarouselSection({ child, imagePaths, sectionId }: PageProps) {
    return <ContentSection
        sectionId={ sectionId }
        child={(
            <div className="md:flex md:flex-row">
                { child }
                <div className="flex items-center md:w-1/2 md:p-6">
                    <Carousel
                        showArrows={false}
                        showStatus={false}
                        showThumbs={false}
                        autoPlay={true}
                        infiniteLoop={true}
                        centerMode={false}
                        dynamicHeight={false}
                    >
                        {imagePaths.map((path, index) => (
                            <div key={index}>
                                <img src={path} alt={`Slide ${index}`} className="max-w-full max-h-full object-cover"/>
                            </div>
                        ))}
                    </Carousel>
                </div>
            </div>
        )}
    />
}

export default CarouselSection;
