import Hobbies from "../components/Hobbies";
import CarouselSection from "./CarouselSection";

const imagePaths: string[] = [
    "images/celeste.jpg",
    "images/piggybank.webp",
    "images/jaco.jpg",
];

function HobbiesSection() {
    return <CarouselSection
        child={(
            <Hobbies />
        )}
        imagePaths={imagePaths}
        sectionId="hobbies"
    />;
}

export default HobbiesSection;
