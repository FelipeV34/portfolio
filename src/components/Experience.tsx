import TextSection from "./TextSection";

const experienceParagraphs = [
    <h2 className="text-xl"><b>GroupM</b> (Jan - July 2019)</h2>,
    <span>
        My first job as a trainee was in a multinational company called <i>GroupM</i>. I
        learned about data processing, pipelines, <b>ETL processes</b> in Data
        Warehouses / Datamarts, and <b>Microsoft SQL Server</b>. I used my
        skills in <b>SQL</b> and <b>Python</b> to get things going, and made a
        small project to automate some of the work for processing CSV and Excel
        files.
    </span>,
    <h2 className="text-xl"><b>GSD PLUS</b> (June 2020 - Jan 2023)</h2>,
    <span>
        In 2020 I worked at a Colombian company called <i>GSD PLUS</i>. It was
        a transportation consultancy company, who's clients were mainly
        governments of some Central American and African countries. I
        learned most of what I do today working for GSD PLUS.
    </span>,
    <span>
        I worked on many projects, starting in 2020 on a project for the
        Colombian Ministry of Transportation, where the consultancy team had
        created a complicated model for estimating nation-wide supply and
        demand of cargo. Our job was to create a web application for them to
        load all the data and run the model. We used the <b>Django</b> framework,
        using the Pandas library on the Back-End to perform the calculations,
        as well as <b>Bootstrap</b> with Jinja templates
        for <b>HTML</b>, <b>CSS</b> and <b>JavaScript</b> for the UI.
    </span>,
    <span>
        I later moved to the main project we had on the development team, which
        was a web application for managing all kinds of data related to the
        transportation sector in Asunción, Paraguay, and it was implemented
        in <b>Angular</b>, <b>Java</b> and <b>Spring Boot</b>, <b>RabbitMQ</b>, <b>PostgreSQL</b> and <b>AWS</b>.
        This project had been in production for a couple years at this point,
        and it was a real challenge, because it had serious scalability issues,
        but eventually me and my colleagues managed to stabilize the
        application. After 4 years in operations, one critical process in the
        project began to slow down tremendously due to poor database design and
        algorithm design: it (unnecessarily) relied on historical data to
        perform some large initial queries. The company did not have the
        resources to maintain this part of the project, so it was eventually
        outsourced to a different company, but I learned the importance of
        designing systems with scalability and maintainability in mind.
    </span>,
    <span>
        I later moved to another project for the Colombian Ministry of
        Transportation, in which we had to create a system for users to measure
        various aspects of their car trips inside the city. Here in Bogotá,
        there are restrictions for circulating motor vehicles on work
        days called <i>Pico y Placa</i>. These restrictions are meant reduce
        traffic by prohibiting certain vehicles from circulating inside the
        city based on their license plates. A colleague and I created the
        Back-End with a simple micro-service architecture and connection
        to <b>Azure B2C</b>, it was implemented
        in <b>C#</b> using <b>.NET 6.0</b>, <b>MongoDB</b> and Azure's <b>Service Bus</b>.
        It was designed to receive large amounts of coordinates from a mobile
        app or GPS devices installed on user vehicles, organize the
        coordinates, detect when a trip has finished, and execute an algorithm
        that I created using linear algebra to eliminate redundant coordinates
        without losing meaningful information.
    </span>,
    <span>
        The last project I worked on at the company was a payment gateway for
        transportation solutions. It had to support different transportation
        payment systems (real-time payments, deferred payments, and aggregated
        payments). This one had a big and complicated micro-service
        architecture, with over thirteen (13) micro-services doing different
        tasks. Similar to the last project, this was done
        using <b>C#</b> and <b>.NET 6.0</b>, <b>Service Bus</b> and <b>MongoDB</b>.
        Initially I worked documenting the project's architecture and
        implementing automated tests in the Back-End, and later made my way
        into the codebase, working alongside the main contributors.
    </span>,
    <h2 className="text-xl"><b>Cisco</b> (Jan 2023 - Dec 2023)</h2>,
    <span>
        During 2023, I worked at <i>Cisco</i>, under the contract of a company
        called <i>Nub 7/8</i>. Our main project was migrating a <i>Tableau</i>
        dashboard to a <i>React</i> application, where I was in charge of the
        Back-End side, and the goal was to make a system where the report could
        load quickly and provide a better user experience than <i>Tableau</i>.
        To achieve this, I used <b>Python</b> with <b>FastAPI</b>, designed a
        simple micro-service architecture with <b>MongoDB</b> for the dasboard
        data and <b>PostgreSQL</b> for other critical data, and I was also in
        charge of the deployment in <b>AWS</b> Lambdas and EC2.
    </span>,
    <span>
        Due to layoffs at <i>Cisco</i>, the company <i>Nub 7/8</i> ceased
        operations on December 22nd, 2023. We were able to deliver what we
        promised in time, but since then, I have been applying for master's degrees
        and getting prepared to continue my education.
    </span>,
];

function Experience() {
    return <TextSection
        title="Experience"
        paragraphs={experienceParagraphs}
    />;
}

export default Experience;
