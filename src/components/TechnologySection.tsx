import TechnologyCard, { TechnologyCardProps } from "./TechnologyCard";

type TechnologySectionProps = {
    technologyTitle: string;
    cardProps: TechnologyCardProps[];
};

function TechnologySection({ technologyTitle, cardProps }: TechnologySectionProps) {
    return (
        <>
            <h2 className="text-2xl"><b>{ technologyTitle }</b></h2>
            <div className="flex flex-wrap justify-left gap-6 py-12">
                {cardProps.map((cardProps, index) => (
                    <TechnologyCard key={index}
                        technologyName={cardProps.technologyName}
                        yearsOfExperience={cardProps.yearsOfExperience}
                        specificKnowledge={cardProps.specificKnowledge}
                        abilityScore={cardProps.abilityScore}
                    />
                ))}
            </div>
        </>
    );
}

export default TechnologySection;
