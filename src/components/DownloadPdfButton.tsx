import { FaFilePdf } from "react-icons/fa";

type DownloadButtonProps = {
    buttonText: string | null,
    onClickAction: () => void,
};

function DownloadPdfButton({ buttonText, onClickAction }: DownloadButtonProps) {
    return (
        <button
            className="flex items-center bg-clSecondary hover:bg-clSecondaryHover py-1 pl-2 pr-4 mr-2 rounded"
            onClick={onClickAction}
        >
            <FaFilePdf className="mr-2" />
            { buttonText }
        </button>
    );
}

export default DownloadPdfButton;
