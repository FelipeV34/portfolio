import TextSection from "./TextSection";

const hobbiesParagraphs = [
    <h2 className="text-xl"><b>Video-games</b></h2>,
    <span>
        Right now I'm playing <a className="text-clAccent
            hover:text-clAccentHover"
            href="https://es.wikipedia.org/wiki/Celeste_(videojuego)"
            target="_blank">Celeste</a> a lot. In this game, the devs embrace
        the speed-running community and integrate some of their tricks on the
        advanced levels, which is fun and extremely challenging. Some other
        games I love are: <a className="text-clAccent
            hover:text-clAccentHover">Hollow Knight</a>, <a
            className="text-clAccent hover:text-clAccentHover"
            href="https://es.wikipedia.org/wiki/This_War_of_Mine"
            target="_blank">This War Of Mine</a>, <a className="ml-1
            text-clAccent hover:text-clAccentHover"
            href="https://es.wikipedia.org/wiki/NieR:_Automata"
            target="_blank">NieR: Automata</a>, <a className="text-clAccent
            hover:text-clAccentHover"
            href="https://es.wikipedia.org/wiki/The_Witcher_3:_Wild_Hunt"
            target="_blank">The Witcher III</a>, and the asymmetrical game <a
            className="text-clAccent hover:text-clAccentHover"
            href="https://es.wikipedia.org/wiki/Dead_by_Daylight"
            target="_blank">Dead by Daylight</a> (even though I no longer play
        it).
    </span>,
    <h2 className="text-xl"><b>Personal finance</b></h2>,
    <span>
        I won't get too deep into this topic here, but I am a personal finance
        freak! I'm very organized when it comes to managing my money, and I
        follow a few personal finance YT channels. Here's some of the best ones
        I know in case you're interested: <a className="text-clAccent
            hover:text-clAccentHover"
            href="https://www.youtube.com/@ThePlainBagel" target="_blank">The
            Plain Bagel</a>, <a className="text-clAccent
            hover:text-clAccentHover"
            href="https://www.youtube.com/@ExitoFinancieroOficial"
            target="_blank">Exito Financiero</a>, and my favorite <a
            className="text-clAccent hover:text-clAccentHover"
            href="https://www.youtube.com/@BenFelixCSI" target="_blank">Ben
            Felix</a>.
    </span>,
    <h2 className="text-xl"><b>Instruments</b></h2>,
    <span>
        I learned to play drums when I was 10 years old, and I loved doing it
        and even had a few bands when I was between 14 and 19. I tried studying
        music in the <i>Fernando Sor Music School</i>, for a little over a
        year, getting better at playing drums, learning theory and a bit of
        piano to visualize harmony (chords) and music theory. However, I
        eventually realized playing music as a job was not for me, and decided
        to switch careers.
    </span>,
    <h2 className="text-xl"><b>Music</b></h2>,
    <span>
        Even though I did not finish my music studies, I learned a lot during
        this time. My harmonic piano teacher was a great influence on the music
        I listen to, recommending jazz artists such as <a
            className="ml-1 text-clAccent hover:text-clAccentHover"
            href="https://es.wikipedia.org/wiki/Jaco_Pastorius"
            target="_blank">Jaco Pastorius</a> (one of the greatest bass
        players of all time) and <a className="text-clAccent
            hover:text-clAccentHover"
            href="https://es.wikipedia.org/wiki/Weather_Report"
            target="_blank">Weather Report</a>, <a className="text-clAccent
            hover:text-clAccentHover"
            href="https://es.wikipedia.org/wiki/Pat_Metheny"
            target="_blank">Pat Metheny</a>, and a few others. I also
        discovered other fantastic jazz artists on my own, such as <a
            className="text-clAccent hover:text-clAccentHover"
            href="https://es.wikipedia.org/wiki/Allan_Holdsworth"
            target="_blank">Allan Holdsworth</a>, <a className="text-clAccent
            hover:text-clAccentHover"
            href="https://es.wikipedia.org/wiki/Yellowjackets"
            target="_blank">Yellowjackets</a>, <a className="text-clAccent
            hover:text-clAccentHover"
            href="https://es.wikipedia.org/wiki/Miles_Davis"
            target="_blank">Miles Davis</a>, <a className="text-clAccent
            hover:text-clAccentHover"
            href="https://es.wikipedia.org/wiki/Herbie_Hancock"
            target="_blank">Herbie Hancock</a>, <a className="text-clAccent
            hover:text-clAccentHover"
            href="https://es.wikipedia.org/wiki/John_Coltrane"
            target="_blank">John Coltrane</a>, <a className="text-clAccent
            hover:text-clAccentHover"
            href="https://es.wikipedia.org/wiki/Steps_Ahead"
            target="_blank">Steps Ahead</a>.
    </span>,
];

function Hobbies() {
    return <TextSection
        title="Hobbies"
        paragraphs={hobbiesParagraphs}
    />;
}

export default Hobbies;
