import Education from "../components/Education";
import CarouselSection from "./CarouselSection";

const imagePaths = [
    "images/fukl.jpg",
    "images/maths.jpg",
    "images/marathon1.jpg",
    "images/marathon2.jpg",
    "images/marathon3.jpg",
];

function EducationSection() {
    return <CarouselSection
        child={ <Education /> }
        imagePaths={ imagePaths }
        sectionId="education"
    />;
}

export default EducationSection;
