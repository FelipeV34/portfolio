import AboutMe from "../components/AboutMe";
import CarouselSection from "../components/CarouselSection";

const imagePaths = [
    "images/bogota.jpg",
    "images/miles.jpg",
];

function AboutMeSection() {
    return <CarouselSection
        child={ <AboutMe /> }
        imagePaths={ imagePaths }
        sectionId="aboutme"
    />;
}

export default AboutMeSection;
