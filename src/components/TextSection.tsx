type SectionProps = {
    title: string,
    paragraphs: JSX.Element[],
}

function TextSection({ title, paragraphs }: SectionProps) {
    return (
        <div className="flex flex-col space-y-12 md:w-1/2 my-12">
            <h1 className="text-4xl">
                <b>{ title }</b>
            </h1>

            {paragraphs.map((paragraph, index) => (
                <div key={index}>
                    {paragraph}
                </div>
            ))}
        </div>
    );
}

export default TextSection;
