import TechnologySection from "../components/TechnologySection";
import ContentSection from "./ContentSection";

function TechnologiesSection() {
    return <ContentSection
        sectionId="technologies"
        child={(
            <div className="flex flex-col mt-12 space-x-12">
                <h1 className="text-4xl pb-12"><b>Technologies</b></h1>

                <TechnologySection
                    technologyTitle="Back-End:"
                    cardProps={[
                        {
                            technologyName: "Python",
                            yearsOfExperience: 4,
                            specificKnowledge: [
                                "Django",
                                "FastAPI",
                                "Flask",
                                "Pandas & GeoPandas",
                            ],
                            abilityScore: 5,
                        },
                        {
                            technologyName: "C#",
                            yearsOfExperience: 2,
                            specificKnowledge: [
                                ".NET 6.0 & 8.0",
                                "LINQ",
                                "Entity Framework",
                            ],
                            abilityScore: 4,
                        },
                        {
                            technologyName: "Java",
                            yearsOfExperience: 2,
                            specificKnowledge: [
                                "SpringBoot",
                                "JPA",
                                "Flowable",
                            ],
                            abilityScore: 2,
                        },
                    ]}
                />

                <TechnologySection
                    technologyTitle="Databases:"
                    cardProps={[
                        {
                            technologyName:"PostgreSQL",
                            yearsOfExperience: 4,
                            abilityScore: 5,
                        },
                        {
                            technologyName: "MongoDB",
                            yearsOfExperience: 2,
                            abilityScore: 4,
                        },
                        {
                            technologyName: "Microsoft SQL Server",
                            yearsOfExperience: 1,
                            abilityScore: 2,
                        }
                    ]}
                />

                <TechnologySection
                    technologyTitle="Cloud:"
                    cardProps={[
                        {
                            technologyName: "AWS",
                            yearsOfExperience: 2,
                            specificKnowledge: [
                                "Lambda",
                                "EC2",
                                "ElasticBeanstalk",
                                "RDS",
                            ],
                            abilityScore: 4,
                        },
                        {
                            technologyName: "Azure",
                            yearsOfExperience: 1,
                            specificKnowledge: [
                                "AKS",
                                "Service Bus",
                            ],
                            abilityScore: 2,
                        }
                    ]}
                />

                <TechnologySection
                    technologyTitle="Other tools:"
                    cardProps={[
                        {
                            technologyName: "IDEs",
                            yearsOfExperience: 4,
                            specificKnowledge: [
                                "Visual Studio Code",
                                "Visual Studio",
                                "PyCharm",
                                "NeoVim",
                            ],
                            abilityScore: 5,
                        },
                        {
                            technologyName: "Linux",
                            yearsOfExperience: "3",
                            specificKnowledge: [
                                "Debian-based",
                                "RedHat-based",
                            ],
                            abilityScore: 5,
                        },
                        {
                            technologyName: "Git",
                            yearsOfExperience: 4,
                            abilityScore: 5,
                        },
                        {
                            technologyName: "Docker",
                            yearsOfExperience: 1,
                            abilityScore: 3,
                        },
                    ]}
                />

                    <TechnologySection
                    technologyTitle="Front-End (learning):"
                    cardProps={[
                        {
                            technologyName: "JavaScript & TypeScript",
                            yearsOfExperience: 0,
                            specificKnowledge: [
                                "Vanilla JS",
                                "React",
                            ],
                            abilityScore: 3,
                        },
                        {
                            technologyName: "HTML & CSS",
                            yearsOfExperience: 0,
                            abilityScore: 2,
                        },
                        {
                            technologyName: "TailwindCSS",
                            yearsOfExperience: 0,
                            abilityScore: 2,
                        }
                    ]}
                />

                </div>
        )}
    />
}

export default TechnologiesSection;
