import { IoIosStar, IoMdStarOutline } from "react-icons/io";

export type TechnologyCardProps = {
    technologyName: string;
    yearsOfExperience: number | string;
    specificKnowledge?: string[];
    abilityScore: 0 | 1 | 2 | 3 | 4 | 5;
};

function TechnologyCard({ technologyName, yearsOfExperience, specificKnowledge, abilityScore }: TechnologyCardProps) {
    const coloredStars: JSX.Element[] = Array.from({ length: abilityScore }, (_, index) => (
        <IoIosStar key={`colored ${index}`} />
    ));

    const uncoloredStars: JSX.Element[] = Array.from({ length: 5 - abilityScore }, (_, index) => (
        <IoMdStarOutline key={`uncolored ${index}`} />
    ));

    return (
        <div className="bg-clSecondaryDark min-w-72 max-w-72 border border-clPrimary rounded-lg p-3">
            <h3 className="flex items-center justify-center text-xl pb-2 border-b border-clPrimary"><b>
                { technologyName }
            </b></h3>
            <span className="flex flex-row p-2 mt-2">
                <b className="pr-2">Years of experience:</b> { yearsOfExperience }
            </span>

            <div className="flex flex-row p-2">
                <b className="pr-2">Ability:</b>
                <div className="flex flex-row pt-1 text-yellow-500">{ coloredStars }</div>
                <div className="flex flex-row pt-1 text-clText">{ uncoloredStars }</div>
            </div>

            {specificKnowledge && (<>
                <div className="p-2 flex flex-col">
                    <b>Specific knowledge:</b>
                </div>
                <ul className="mb-2">
                    {specificKnowledge.map((libOrFramework, index) => (
                        <li key={index} className="ml-4">
                            ✅ {libOrFramework}
                        </li>
                    ))}
                </ul>
            </>)}

        </div>
    );
}

export default TechnologyCard;
