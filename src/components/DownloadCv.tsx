import { toast } from "react-toastify";
import DownloadPdfButton from "./DownloadPdfButton";

const filePaths = {
    english: "cv/felipe-vanegas-CV-english.pdf",
    spanish: "cv/felipe-vanegas-CV-espanol.pdf",
};

function DownloadCv() {
    function downloadPdf(filePath: string) {
        fetch(filePath)
            .then(response => {
                const contentType = response.headers.get("Content-Type");
                if (!contentType || !contentType.includes("application/pdf")) {
                    const errorMessage = "Unable to locate PDF file";
                    toast.error(errorMessage);
                    throw new Error(errorMessage);
                } else {
                    return response.blob();
                }
            })
            .then(blob => {
                const url = window.URL.createObjectURL(new Blob([blob]));
                const link = document.createElement("a");
                link.href = url;
                link.setAttribute("download", filePath);
                document.body.appendChild(link);
                link.click();
                link.parentNode?.removeChild(link);
            })
            .catch(error => {
                console.log(error);
            });
    };

    return (
        <div className="flex">
            <DownloadPdfButton
                buttonText="English"
                onClickAction={() => downloadPdf(filePaths.english)}
            />
            <DownloadPdfButton
                buttonText={"Español"}
                onClickAction={() => downloadPdf(filePaths.spanish)}
            />
        </div>
    );
}

export default DownloadCv;
